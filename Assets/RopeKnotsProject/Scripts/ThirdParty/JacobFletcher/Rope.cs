using System;
using UnityEngine;

namespace RopeKnotsProject.ThirdParty.JacobFletcher
{
// Require a Rigidbody and LineRenderer object for easier assembly
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(LineRenderer))]
    public class Rope : MonoBehaviour
    {
        /*========================================
        ==  Physics Based Rope				==
        ==  File: Rope.js					  ==
        ==  Original by: Jacob Fletcher		==
        ==  Use and alter Freely			 ==
        ==  CSharp Conversion by: Chelsea Hash  ==
        == https://web.archive.org/web/20200221080842/http://wiki.unity3d.com/index.php/LineRenderer_Rope ==
        ==========================================
        How To Use:
         ( BASIC )
         1. Simply add this script to the object you want a rope teathered to
         2. In the "LineRenderer" that is added, assign a material and adjust the width settings to your likeing
         3. Assign the other end of the rope as the "Target" object in this script
         4. Play and enjoy!
     
         ( Advanced )
         1. Do as instructed above
         2. If you want more control over the rigidbody on the ropes end go ahead and manually
             add the rigidbody component to the target end of the rope and adjust acordingly.
         3. adjust settings as necessary in both the rigidbody and rope script
     
         (About Character Joints)
         Sometimes your rope needs to be very limp and by that I mean NO SPRINGY EFFECT.
         In order to do this, you must loosen it up using the swingAxis and twist limits.
         For example, On my joints in my drawing app, I set the swingAxis to (0,0,1) sense
         the only axis I want to swing is the Z axis (facing the camera) and the other settings to around -100 or 100.
     
     
        */

        public Transform target;
        //  Sets the amount of joints there are in the rope (1 = 1 joint for every 1 unit)
        public float resolution = 0.5F;
        //  Sets each joints Drag
        public float ropeDrag = 0.1F;
        //  Sets each joints Mass
        public float ropeMass = 0.1F;
        //  Sets the radius of the collider in the SphereCollider component
        public float ropeColRadius = 0.5F;
        //public float ropeBreakForce = 25.0F;
        ////-------------- TODO (Hopefully will break the rope in half...

        //  DON'T MESS!	This is for the Line Renderer's Reference and to set up the positions of the gameObjects
        private Vector3[] segmentPos;
        //  DON'T MESS!	This is the actual joint objects that will be automatically created
        //  DON'T MESS!	 The line renderer variable is set up when its assigned as a new component
        private GameObject[] joints;
        //  DON'T MESS!	The number of segments is calculated based off of your distance * resolution
        private LineRenderer line;
        //  DON'T MESS!	This is to keep errors out of your debug window! Keeps the rope from rendering when it doesnt exist...
        private int segments;
        private bool rope;

        //Joint Settings
        //  Sets which axis the character joint will swing on (1 axis is best for 2D, 2-3 axis is best for 3D (Default= 3 axis))
        public Vector3 swingAxis = new Vector3(1, 1, 1);
        //  The lower limit around the primary axis of the character joint.
        public float lowTwistLimit = -100.0F;
        //  The upper limit around the primary axis of the character joint.
        public float highTwistLimit = 100.0F;
        //	The limit around the primary axis of the character joint starting at the initialization point.
        public float swing1Limit = 20.0F;

        private void Awake()
        {
            BuildRope();
        }

        private void Update()
        {
            // Put rope control here!


            //Destroy Rope Test	(Example of how you can use the rope dynamically)
            if (rope && Input.GetKeyDown("d"))
            {
                DestroyRope();
            }

            if (!rope && Input.GetKeyDown("r"))
            {
                BuildRope();
            }
        }

        private void LateUpdate()
        {
            // Does rope exist? If so, update its position
            if (rope)
            {
                for (var i = 0; i < segments; i++)
                {
                    if (i == 0)
                    {
                        line.SetPosition(i, transform.position);
                    }
                    else if (i == segments - 1)
                    {
                        line.SetPosition(i, target.transform.position);
                    }
                    else
                    {
                        line.SetPosition(i, joints[i].transform.position);
                    }
                }

                line.enabled = true;
            }
            else
            {
                line.enabled = false;
            }
        }

        private void BuildRope()
        {
            line = gameObject.GetComponent<LineRenderer>();

            // Find the amount of segments based on the distance and resolution
            // Example: [resolution of 1.0 = 1 joint per unit of distance]
            var targetPosition = target.position;
            var selfPosition = transform.position;
            segments = (int)(Vector3.Distance(selfPosition, targetPosition) * resolution);
            line.positionCount = segments;
            segmentPos = new Vector3[segments];
            joints = new GameObject[segments];
            segmentPos[0] = selfPosition;
            segmentPos[segments - 1] = targetPosition;

            // Find the distance between each segment
            var segmentCount = segments - 1;
            var separation = (targetPosition - selfPosition) / segmentCount;

            for (var s = 1; s < segments; s++)
            {
                // Find the each segments position using the slope from above
                var vector = (separation * s) + transform.position;
                segmentPos[s] = vector;

                //Add Physics to the segments
                AddJointPhysics(s);
            }

            // Attach the joints to the target object and parent it to this object	
            var end = target.gameObject.AddComponent<CharacterJoint>();
            end.connectedBody = joints[^1].transform.GetComponent<Rigidbody>();
            end.swingAxis = swingAxis;
            var limitSetter = end.lowTwistLimit;
            limitSetter.limit = lowTwistLimit;
            end.lowTwistLimit = limitSetter;
            limitSetter = end.highTwistLimit;
            limitSetter.limit = highTwistLimit;
            end.highTwistLimit = limitSetter;
            limitSetter = end.swing1Limit;
            limitSetter.limit = swing1Limit;
            end.swing1Limit = limitSetter;
            target.parent = transform;

            // Rope = true, The rope now exists in the scene!
            rope = true;
        }

        private void AddJointPhysics(int n)
        {
            joints[n] = new GameObject("Joint_" + n);
            joints[n].transform.parent = transform;
            var rigid = joints[n].AddComponent<Rigidbody>();
            var col = joints[n].AddComponent<SphereCollider>();
            var ph = joints[n].AddComponent<CharacterJoint>();
            ph.swingAxis = swingAxis;
            var limitSetter = ph.lowTwistLimit;
            limitSetter.limit = lowTwistLimit;
            ph.lowTwistLimit = limitSetter;
            limitSetter = ph.highTwistLimit;
            limitSetter.limit = highTwistLimit;
            ph.highTwistLimit = limitSetter;
            limitSetter = ph.swing1Limit;
            limitSetter.limit = swing1Limit;
            ph.swing1Limit = limitSetter;
            //ph.breakForce = ropeBreakForce; <--------------- TODO

            joints[n].transform.position = segmentPos[n];

            rigid.drag = ropeDrag;
            rigid.mass = ropeMass;
            col.radius = ropeColRadius;

            ph.connectedBody = n == 1
                ? transform.GetComponent<Rigidbody>()
                : joints[n - 1].GetComponent<Rigidbody>();
        }

        private void DestroyRope()
        {
            // Stop Rendering Rope then Destroy all of its components
            rope = false;
            for (var dj = 0; dj < joints.Length - 1; dj++)
            {
                Destroy(joints[dj]);
            }

            segmentPos = Array.Empty<Vector3>();
            joints = Array.Empty<GameObject>();
            segments = 0;
        }
    }
}