using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RopeKnotsProject.ThirdParty.JacobFletcher
{
    public class TubeVertex
    {
#if TUBE_JFLETCHER
        public Vector3 Point;
        public float Radius;

        public TubeVertex(Vector3 pt, float r)
        {
            Point = pt;
            Radius = r;
        }
#endif
    }

    /// <summary>
    /// TubeRenderer.js
    ///     This script is created by Ray Nothnagel of Last Bastion Games. It is
    ///     free for use and available on the Unify Wiki.
    ///     For other components I've created, see:
    /// http://lastbastiongames.com/middleware/
    /// (C) 2008 Last Bastion Games
    ///     --------------------------------------------------------------
    /// EDIT: MODIFIED BY JACOB FLETCHER FOR USE WITH THE ROPE SCRIPT
    /// http://www.reverieinteractive.com | https://github.com/dajver/Rope-Unity3d-Examples/blob/master/Assets/RopeExamples/Rope4/TubeRenderer.cs
    ///
    /// MORE EDIT: MODIFIED BY SHIVAM "VIVRAAN" MUKHERJEE
    /// https://linkedin.com/in/shivammukherjee
    /// </summary>
    [RequireComponent(typeof(MeshFilter)), RequireComponent(typeof(MeshRenderer))]
    public class TubeRenderer : MonoBehaviour
    {
#if TUBE_JFLETCHER
        private static readonly int BumpMap = Shader.PropertyToID("_BumpMap");

        [SerializeField] private Material material;
        [SerializeField] private int crossSegments = 3;
        [SerializeField] private bool useMeshCollision;
        [SerializeField] private float endOffsetBias = 0.01f;

        private TubeVertex[] vertices;
        private Vector3 lastCameraPosition1;
        private Vector3 lastCameraPosition2;
        private Vector3[] crossPoints;
        private int lastCrossSegments;
        private Mesh mesh;
        private MeshCollider meshCollider;
        private MeshRenderer meshRenderer;
        private MeshFilter meshFilter;
        private bool usingBumpmap;

        private int lastUpdateHash;

        public void Reset()
        {
            mesh = new Mesh();
            vertices = new TubeVertex[2];
            vertices[0] = new TubeVertex(Vector3.zero, 1.0f);
            vertices[1] = new TubeVertex(Vector3.right, 1.0f);
            lastUpdateHash = PropHashCode();
        }

        private void Start()
        {
            Reset();
            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<MeshRenderer>();
            meshRenderer.material = material;
            if (!material) return;
            if (material.GetTexture(BumpMap)) usingBumpmap = true;
        }

        private void LateUpdate()
        {
            if (lastUpdateHash == PropHashCode()) return;
            meshFilter.mesh = CreateMesh();

            Mesh CreateMesh()
            {
                if (vertices.Length <= 1)
                {
                    meshRenderer.enabled = false;
                    return mesh;
                }

                meshRenderer.enabled = true;
                if (crossSegments != lastCrossSegments)
                {
                    crossPoints = new Vector3[crossSegments];
                    var theta = 2 * Mathf.PI / crossSegments;
                    for (var c = 0; c < crossSegments; c++)
                    {
                        crossPoints[c] = new Vector3(Mathf.Cos(theta * c), Mathf.Sin(theta * c), 0);
                    }

                    lastCrossSegments = crossSegments;
                }

                var meshVertices = new Vector3[vertices.Length * crossSegments];
                var uvs = new Vector2[vertices.Length * crossSegments];
                var tris = new int[vertices.Length * crossSegments * 6];
                var lastVertices = new int[crossSegments];
                var theseVertices = new int[crossSegments];
                var rotation = Quaternion.identity;

                for (var p = 0; p < vertices.Length; p++)
                {
                    if (p < vertices.Length - 1)
                        rotation = Quaternion.FromToRotation(
                            Vector3.forward,
                            vertices[p + 1].Point - vertices[p].Point);

                    for (var c = 0; c < crossSegments; c++)
                    {
                        var vertexIndex = p * crossSegments + c;
                        meshVertices[vertexIndex] = vertices[p].Point + rotation * crossPoints[c] * vertices[p].Radius;
                        uvs[vertexIndex] = new Vector2(c / (float)crossSegments, p / (float)vertices.Length);
                        lastVertices[c] = theseVertices[c];
                        theseVertices[c] = p * crossSegments + c;
                    }

                    //make triangles
                    if (p <= 0) continue;
                    for (var c = 0; c < crossSegments; c++)
                    {
                        var start = (p * crossSegments + c) * 6;
                        tris[start] = lastVertices[c];
                        tris[start + 1] = lastVertices[PositiveMod((c + 1), crossSegments)];
                        tris[start + 2] = theseVertices[c];
                        
                        tris[start + 3] = tris[start + 2];
                        tris[start + 4] = tris[start + 1];
                        tris[start + 5] = theseVertices[PositiveMod((c + 1), crossSegments)];
                    }

                    int PositiveMod(int m, int n) => (m % n + n) % n;
                }

                //Clear mesh for new build  (jf)   
                mesh.Clear();
                mesh.vertices = meshVertices;
                mesh.triangles = tris;
                mesh.RecalculateNormals();
                if (usingBumpmap)
                    mesh.tangents = CalculateTangents(meshVertices);
                mesh.uv = uvs;

                if (!useMeshCollision) return mesh;

                if (meshCollider)
                {
                    meshCollider.sharedMesh = mesh;
                }
                else
                {
                    meshCollider = gameObject.AddComponent<MeshCollider>();
                }

                return mesh;
            }
        }


        private static Vector4[] CalculateTangents(IReadOnlyList<Vector3> verts)
        {
            var tangents = new Vector4[verts.Count];

            for (var i = 0; i < tangents.Length; i++)
            {
                var vertex1 = i > 0 ? verts[i - 1] : verts[i];
                var vertex2 = i < tangents.Length - 1 ? verts[i + 1] : verts[i];
                var tan = (vertex1 - vertex2).normalized;
                tangents[i] = new Vector4(tan.x, tan.y, tan.z, 1.0f);
            }

            return tangents;
        }


        //sets all the points to points of a Vector3 array, as well as capping the ends.
        public void SetPoints(IReadOnlyList<Vector3> points, float radius)
        {
            if (points.Count < 2) return;
            vertices = new TubeVertex[points.Count + 2];

            var v0Offset = (points[0] - points[1]) * endOffsetBias;
            vertices[0] = new TubeVertex(v0Offset + points[0], 0.0f);
            var v1Offset = (points[^1] - points[^2]) * endOffsetBias;
            vertices[^1] = new TubeVertex(v1Offset + points[^1], 0.0f);

            for (var p = 0; p < points.Count; p++)
            {
                vertices[p + 1] = new TubeVertex(points[p], radius);
            }
        }

        private int PropHashCode()
        {
            return vertices.Aggregate(0, (total, it) => total ^ it.GetHashCode()) ^ vertices.GetHashCode() ^
                   crossSegments.GetHashCode();
        }
#endif
    }
}