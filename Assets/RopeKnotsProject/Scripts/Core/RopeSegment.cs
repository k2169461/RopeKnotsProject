using System;
using System.Collections.Generic;
using UnityEngine;

namespace RopeKnotsProject.Core
{
    public class RopeSegment : MonoBehaviour
    {
        [SerializeField, Range(-1f, 1f), Tooltip("Normalized fraction used for computing when the distance between two segments should be considered as unstable (segments start acting crazy).")]
        private float instabilityAllowance = 0.1f;
        [SerializeField] private MeshRenderer[] sausageRenderers;
        public IEnumerable<MeshRenderer> SausageRenderers => sausageRenderers;

        private RopeBody ropeBody;

        [Obsolete("Use property.")]
        private CapsuleCollider capsuleCollider;
        public CapsuleCollider CapsuleCollider
        {
            get
            {
#pragma warning disable CS0618
                if (!capsuleCollider)
                {
                    return capsuleCollider = GetComponent<CapsuleCollider>();
                }

                return capsuleCollider;
#pragma warning restore CS0618
            }
        }

        [Obsolete("Use property.")]
        private Rigidbody rb;
        public Rigidbody Rigidbody
        {
            get
            {
#pragma warning disable CS0618
                if (!rb)
                {
                    return rb = GetComponent<Rigidbody>();
                }

                return rb;
#pragma warning restore CS0618
            }
        }

        [Obsolete("Use property.")]
        private ConfigurableJoint configurableJoint;
        public ConfigurableJoint ConfigurableJoint
        {
            get
            {
#pragma warning disable CS0618
                if (!configurableJoint)
                {
                    return configurableJoint = GetComponent<ConfigurableJoint>();
                }

                return configurableJoint;
#pragma warning restore CS0618
            }
        }

        public void Setup(in RopeBody owningRopeBody)
        {
            ropeBody = owningRopeBody;
        }

        private void Update()
        {
#pragma warning disable CS0618
            if (!configurableJoint) return;
#pragma warning restore CS0618
            
            var permissibleDistance =
                (CapsuleCollider.height * ropeBody.RopeNormalizedSpawnPoint - ropeBody.RopeSpawnPointBias) *
                (1 + instabilityAllowance);
            var actualDistanceSqr = 
                (transform.position - ConfigurableJoint.connectedBody.transform.position).sqrMagnitude;
            if (permissibleDistance * permissibleDistance <= actualDistanceSqr)
            {
                ropeBody.onSegmentsDestabilised.Invoke();
            }
        }
    }
}
