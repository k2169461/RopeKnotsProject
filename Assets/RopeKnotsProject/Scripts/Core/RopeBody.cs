using System;
using System.Collections.Generic;
using System.Linq;
using RopeKnotsProject.ThirdParty.JacobFletcher;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Pool;

namespace RopeKnotsProject.Core
{
    /// <summary>
    /// Generates ropes and acts as the root space for all generated rope segments.
    /// </summary>
    public class RopeBody : MonoBehaviour
    {
        [SerializeField] private Transform ropeSpace;

        [Header("Rope Segment Setup")]
        [SerializeField] private RopeSegment segmentPrefab;
        [SerializeField] private RopeSegment endSegmentPrefab;
        [SerializeField] private int maxSegmentsToSpawn = 15;
        [SerializeField, Range(0, 1)] private float ropeNormalizedSpawnPoint = 1;
        public float RopeNormalizedSpawnPoint => ropeNormalizedSpawnPoint;
        [SerializeField] private float ropeSpawnPointBias;
        public float RopeSpawnPointBias => ropeSpawnPointBias;
        [SerializeField] private bool shouldUseTubeRenderer = true;

        [Header("Events")]
        public UnityEvent onSegmentsDestabilised;

        private bool spawnPointAdjustmentSetupDone;
        // Set this to a reasonable default
        private float segmentHeight = 0.2f;
        private Vector3 nextSpawnPoint;
        private Vector3 spawnPointAdjustment;
        private IObjectPool<RopeSegment> ropeSegmentsPool;
        private List<RopeSegment> segments;
        public IReadOnlyList<RopeSegment> Segments => segments;

        // Can be cached if the rope segments are not swapped around 
        private IReadOnlyList<Rigidbody> segmentRigidbodies;
        public IReadOnlyList<Rigidbody> SegmentRigidbodies =>
            segmentRigidbodies ??= Segments.Select(segment => segment.Rigidbody).ToArray();

        [Obsolete("Use property.")]
        private RopeSegment endSegment;
        private RopeSegment EndSegment
        {
            get
            {
#pragma warning disable CS0618
                if (!endSegment)
                {
                    return endSegment = Instantiate(endSegmentPrefab, ropeSpace);
                }

                return endSegment;
#pragma warning restore CS0618
            }
        }

        [Obsolete("Use property.")]
        private TubeRenderer tubeRenderer;
        private TubeRenderer TubeRenderer
        {
            get
            {
#pragma warning disable CS0618
                if (!tubeRenderer)
                {
                    return tubeRenderer = GetComponent<TubeRenderer>();
                }

                return tubeRenderer;
#pragma warning restore CS0618
            }
        }

        [Obsolete("Use property.")]
        private RopeSplineModel ropeSplineModel;
        private RopeSplineModel RopeSplineModel
        {
            get
            {
#pragma warning disable CS0618
                if (!ropeSplineModel)
                {
                    return ropeSplineModel = GetComponent<RopeSplineModel>();
                }

                return ropeSplineModel;
#pragma warning restore CS0618
            }
        }

        private void OnEnable()
        {
            EditorDestroyRopeSegments();
            GenerateRopeSegments();
        }

        /// <summary>
        /// Allows for testing rope generation without physics simulation in-editor.
        /// </summary>
        [ContextMenu(nameof(GenerateRopeSegments))]
        private void GenerateRopeSegments()
        {
            for (var i = 0; i < maxSegmentsToSpawn; i++)
            {
                GenerateSingleRopeSegment(i == maxSegmentsToSpawn - 1);
            }

            ControlTubeRenderer();

            void GenerateSingleRopeSegment(in bool shouldEnd)
            {
                if (ropeSegmentsPool == null)
                {
                    InitRopeSegmentsPool();
                }

                if (!shouldEnd)
                {
                    ropeSegmentsPool?.Get();
                    return;
                }

                EndSegment.gameObject.SetActive(true);
                InitSegment(EndSegment);
            }
        }

        private void FixedUpdate() => ControlTubeRenderer();

        private void ControlTubeRenderer()
        {
            if (!shouldUseTubeRenderer || RopeSplineModel.SplinePoints == null) return;
            TubeRenderer.SetPoints(RopeSplineModel.SplinePoints, segmentPrefab.CapsuleCollider.radius);
        }

        /// <summary>
        /// Destroys all rope segments in-editor.
        /// </summary>
        [ContextMenu("Destroy Rope Segments")]
        private void EditorDestroyRopeSegments()
        {
#if UNITY_EDITOR
            segments?.Clear();
            ropeSegmentsPool?.Clear();

            foreach (var segment in GetComponentsInChildren<RopeSegment>())
            {
                DestroyImmediate(segment.gameObject);
            }

            DestroyImmediate(EndSegment.gameObject);

            nextSpawnPoint = transform.position;
#endif
        }

        private void InitRopeSegmentsPool()
        {
            ropeSegmentsPool = new ObjectPool<RopeSegment>(
                CreateFunc,
                ActionOnGet,
                ActionOnRelease,
                ActionOnDestroy,
                true, maxSegmentsToSpawn
            );

            RopeSegment CreateFunc()
            {
                var segment = Instantiate(segmentPrefab, ropeSpace);
                segment.Setup(this);
                return segment;
            }

            void ActionOnGet(RopeSegment segmentToGet)
            {
                segmentToGet.gameObject.SetActive(true);
                InitSegment(segmentToGet);
            }

            void ActionOnDestroy(RopeSegment segmentToDestroy)
            {
                DeleteSegment(segmentToDestroy);
                Destroy(segmentToDestroy.gameObject);
            }

            void ActionOnRelease(RopeSegment segmentToRelease)
            {
                DeleteSegment(segmentToRelease);
                segmentToRelease.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Initializes a single segment. Is used from within the Object Pool callbacks.
        /// </summary>
        /// <param name="segment"></param>
        private void InitSegment(in RopeSegment segment)
        {
            SetupSpawnPoint();
            EnableSausageRenderers(segment, enable: true);

            segment.Rigidbody.isKinematic = false;
            var segmentTransform = segment.transform;
            segmentTransform.position = nextSpawnPoint;
            segmentTransform.rotation = Quaternion.identity;
            nextSpawnPoint += spawnPointAdjustment;

            var shouldEnd = false;
            if (segment.ConfigurableJoint)
            {
                segment.ConfigurableJoint.connectedBody = null;
            }
            else
            {
                shouldEnd = true;
            }

            segments ??= new List<RopeSegment>();
            segments.Add(segment);

            if (segments.Count > 1 && segments[^2].ConfigurableJoint)
            {
                segments[^2].ConfigurableJoint.connectedBody = segment.Rigidbody;
            }

            segment.name = shouldEnd ? "RopeEndSegment" : $"RopeSegment_{segments.Count:000}";

            // Works okay in the Editor Scene View
            void SetupSpawnPoint()
            {
                if (spawnPointAdjustmentSetupDone)
                    return;

                nextSpawnPoint = ropeSpace.position;
                // Prefer prefab setup for segmentHeight
                segmentHeight = segmentPrefab.CapsuleCollider.height;
                // Combine the normalized size and bias for more control over spawning
                spawnPointAdjustment = new Vector3(0, segmentHeight * ropeNormalizedSpawnPoint + ropeSpawnPointBias, 0);
                spawnPointAdjustmentSetupDone = true;
            }
        }

        [ContextMenu("Clear Spawn Point Adjustment Setup Flag")]
        private void ClearSpawnPointAdjustmentSetupFlag()
        {
#if UNITY_EDITOR
            print("spawnPointAdjustmentSetupDone = false");
            spawnPointAdjustmentSetupDone = false;
#endif
        }


        /// <summary>
        /// Deletes a single segment. Is used from within the Object Pool callbacks.
        /// </summary>
        private void DeleteSegment(in RopeSegment segment)
        {
            nextSpawnPoint -= spawnPointAdjustment;
            EnableSausageRenderers(segment, enable: false);

            var rb = segment.Rigidbody;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            rb.isKinematic = true;
        }

        private void EnableSausageRenderers(in RopeSegment segment, in bool enable)
        {
            if (shouldUseTubeRenderer) return;
            foreach (var sausageRenderer in segment.SausageRenderers)
            {
                sausageRenderer.enabled = enable;
            }
        }
    }
}