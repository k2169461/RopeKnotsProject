using System;
using System.Collections.Generic;
using UnityEngine;

namespace RopeKnotsProject.Core
{
    /// <summary>
    /// Class that manages generating an interpolation of the rope as a Centripetal Catmull-Rom Curve.
    /// Also used for recording spline data to be used in ML training.
    /// </summary>
    [RequireComponent(typeof(RopeBody))]
    public class RopeSplineModel : MonoBehaviour
    {
        [Header("Spline generation")]
        [SerializeField, Range(0f, 1f), Tooltip("Spline alpha parameter. 0.5 for Centripetal Catmull-Rom Splines.")]
        private float alpha = 0.5f;
        [SerializeField, Min(2), Tooltip("Number of spline points to add between the centers of two segments.")]
        private int splineDetailLevel = 32;
        [SerializeField, Min(0.0001f),
         Tooltip("The distance to be added to fake a point past the last segment's position.")]
        private float endCapBias = 0.1f;
        [SerializeField, Min(1f), Tooltip("Amount the end cap deviates from the spline curve.")]
        private float endCapBiasSpread = 1.5f;

        private Vector3[] splinePoints;
        public IReadOnlyList<Vector3> SplinePoints => splinePoints;
        private Vector3 endCapBiasVec;

        [Obsolete("Use property.")]
        private RopeBody ropeBody;
        private RopeBody RopeBody
        {
            get
            {
#pragma warning disable CS0618
                if (!ropeBody)
                {
                    return ropeBody = GetComponent<RopeBody>();
                }

                return ropeBody;
#pragma warning restore CS0618
            }
        }

        private void Start()
        {
            splinePoints = new Vector3[(RopeBody.Segments.Count - 1) * splineDetailLevel];
            endCapBiasVec = endCapBias * Vector3.one.normalized;
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (RopeBody.Segments == null || RopeBody.Segments.Count == 0) return;
            
            /*
             A representation of how the curve is processed as a window of points.
             0 1 2 3 4 5 6 7 8 9
            {- - - -}| | |
            | {- - - -}| |
            | | {- - - -}|
            | | | {- - - -}
            | | | | {- - - -}
            | | | | | {- - - -}
            | | | | | | {- - - -} 
             0 1 2 3 4 5 6 7 8 9
             ~0 1 2 3 4 5~{- - - -}
            */

            // Starting cap
            AssignPoint(0, new CentripetalCatmullRomCurve(
                GetSegmentPosition(0) - endCapBiasVec * Mathf.Log(endCapBiasSpread),
                GetSegmentPosition(0) - endCapBiasVec,
                GetSegmentPosition(1),
                GetSegmentPosition(2), alpha));

            var numSplineWindows = RopeBody.Segments.Count - 3;
            for (var i = 1; i <= numSplineWindows; i++)
            {
                AssignPoint(i, new CentripetalCatmullRomCurve(
                    GetSegmentPosition(i - 1 + 0),
                    GetSegmentPosition(i - 1 + 1),
                    GetSegmentPosition(i - 1 + 2),
                    GetSegmentPosition(i - 1 + 3), alpha));
            }

            // Ending cap
            AssignPoint(numSplineWindows + 1, new CentripetalCatmullRomCurve(
                GetSegmentPosition(numSplineWindows),
                GetSegmentPosition(numSplineWindows + 1),
                GetSegmentPosition(numSplineWindows + 2) + endCapBiasVec,
                GetSegmentPosition(numSplineWindows + 2) + endCapBiasVec * Mathf.Log(endCapBiasSpread),
                alpha));

            void AssignPoint(in int i, in CentripetalCatmullRomCurve ccrCurve)
            {
                for (var j = 0; j < splineDetailLevel; j++)
                {
                    splinePoints[i * splineDetailLevel + j] = ccrCurve.GetPoint(t: j / (float)splineDetailLevel);
                }
            }

            // Can throw index out of range errors when the rope has no segments
            Vector3 GetSegmentPosition(in int i) => RopeBody.SegmentRigidbodies[i].position;
        }

        /// <summary>
        /// Helps visualize the spline points in the Editor.
        /// </summary>
        private void OnDrawGizmos()
        {
            if (splinePoints == null || splinePoints.Length == 0) return;

            foreach (var point in splinePoints)
            {
                Gizmos.color = Color.white;
                Gizmos.DrawWireSphere(point, 0.01f);
            }
        }
    }

    /// <summary>
    /// From https://en.wikipedia.org/wiki/Centripetal_Catmull%E2%80%93Rom_spline#Code_example_in_Unity_C#
    /// </summary>
    public readonly struct CentripetalCatmullRomCurve
    {
        private readonly Vector3 p0, p1, p2, p3;
        private readonly float alpha;

        public CentripetalCatmullRomCurve(in Vector3 p0, in Vector3 p1, in Vector3 p2, in Vector3 p3, in float alpha)
        {
            (this.p0, this.p1, this.p2, this.p3) = (p0, p1, p2, p3);
            this.alpha = alpha;
        }

        /// <summary>
        /// Evaluates a point at given t-value.
        /// </summary>
        /// <param name="t">A parameter between 0 and 1.</param>
        /// <returns>A point on the CCR curve.</returns>
        public Vector3 GetPoint(float t)
        {
            // calculate knots
            const float k0 = 0;
            var k1 = GetKnotInterval(p0, p1, alpha) + k0;
            var k2 = GetKnotInterval(p1, p2, alpha) + k1;
            var k3 = GetKnotInterval(p2, p3, alpha) + k2;

            // evaluate the point
            var u = Mathf.LerpUnclamped(k1, k2, t);
            // ReSharper disable InconsistentNaming
            var A1 = Remap(k0, k1, p0, p1, u);
            var A2 = Remap(k1, k2, p1, p2, u);
            var A3 = Remap(k2, k3, p2, p3, u);
            var B1 = Remap(k0, k2, A1, A2, u);
            var B2 = Remap(k1, k3, A2, A3, u);
            // ReSharper restore InconsistentNaming
            return Remap(k1, k2, B1, B2, u);

            static float GetKnotInterval(Vector3 a, Vector3 b, float alpha) =>
                Mathf.Pow(Vector3.SqrMagnitude(a - b), 0.5f * alpha);

            static Vector3 Remap(float a, float b, Vector3 c, Vector3 d, float u) =>
                Vector3.LerpUnclamped(c, d, (u - a) / (b - a));
        }
    }
}