using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RopeKnotsProject.Core
{
    public class RopeRecorder : MonoBehaviour
    {
        private enum Label
        {
            NoKnot,
            Overhand,
            Bowline,
            Constrictor
        }

        // To be extra clear
        private const int NumVec3 = 3;
        [SerializeField] private int numRecordedRows = 1 << 9;
        [SerializeField] private Label recordingLabel = Label.NoKnot;
        [SerializeField] private string saveDirectoryRelativePath = "MLData/";
        [SerializeField] private string fileNameBase = "Recording";
        [SerializeField] private string fileNameFormat = "{0}_{1:000}.csv";
        [SerializeField, Min(0), Tooltip("in seconds")]
        private int fileWriteInterval = 10;

        [Obsolete("Use property.")]
        private RopeBody ropeBody;
        private RopeBody RopeBody
        {
            get
            {
#pragma warning disable CS0618
                if (!ropeBody)
                {
                    return ropeBody = GetComponent<RopeBody>();
                }

                return ropeBody;
#pragma warning restore CS0618
            }
        }

        private float[] recordings;
        private Vector3[] segmentPosDiffs;
        private int rowWriteHead;
        private StringBuilder recordingTextBuilder;
        private float? recordingIntervalStart;
        private double? recordingSessionStart;
        private string recordingFilePath;
        private int NumSegments => RopeBody.Segments.Count;

        private void Start()
        {
            recordings = new float[NumSegments * NumVec3 * numRecordedRows];
            segmentPosDiffs = new Vector3[NumSegments];
        }

        private void FixedUpdate()
        {
            recordingSessionStart ??= Time.timeAsDouble;
            recordingIntervalStart ??= Time.time;
            RecordRopeOnce(
                NumSegments, RopeBody.SegmentRigidbodies, ref segmentPosDiffs, ref recordings, ref rowWriteHead);

            if (rowWriteHead < numRecordedRows) return;
            var interval = Time.time - recordingIntervalStart!.Value;
            Debug.LogFormat("record interval in seconds={0:F2}", interval);
            recordingIntervalStart = Time.time;

            WriteToStringBuilder(
                numRecordedRows, NumSegments, interval, fileWriteInterval, recordingLabel,
                recordings, ref recordingTextBuilder);
            rowWriteHead = 0;

            // Round to nearest hundred's place
            var roundedTimeStamp = (long)Math.Round(Time.timeAsDouble - recordingSessionStart!.Value);
            if (roundedTimeStamp > 0L && roundedTimeStamp % fileWriteInterval == 0L)
            {
                WriteToFile();
            }

            static void RecordRopeOnce(
                in int numSegments,
                in IReadOnlyList<Rigidbody> segmentRbs,
                ref Vector3[] segmentPosDiffs,
                ref float[] recordings,
                ref int rowWriteHead)
            {
                segmentPosDiffs[0] = Vector3.zero;
                for (var i = 1; i < numSegments; i++)
                {
                    var prev = segmentRbs[i - 1].position;
                    var curr = segmentRbs[i + 0].position;
                    segmentPosDiffs[i] = curr - prev;
                }

                var rowStartPos = rowWriteHead * NumVec3 * numSegments;

                for (var i = 0; i < numSegments; i++)
                {
                    recordings[rowStartPos + i] = segmentPosDiffs[i].x;
                }

                for (var i = 0; i < numSegments; i++)
                {
                    recordings[rowStartPos + i + numSegments] = segmentPosDiffs[i].y;
                }

                for (var i = 0; i < numSegments; i++)
                {
                    recordings[rowStartPos + i + 2 * numSegments] = segmentPosDiffs[i].z;
                }

                rowWriteHead += 1;
            }

            static void WriteToStringBuilder(
                in int numRecordedRows,
                in int numSegments,
                in float recordingInterval,
                in int fileWriteInterval,
                in Label recordingLabel,
                in IReadOnlyList<float> recordings,
                ref StringBuilder recordingTextBuilder
            )
            {
                const string unsignedEntryLike = "0.000000";
                var labelStr = recordingLabel.ToString();
                var m = numSegments * NumVec3;

                const int signCharLen = 1, newCharLen = 1, commaLen = 1;
                var unsignedEntryLen = unsignedEntryLike.Length;
                var labelLen = labelStr.Length;
                var numBlocks = (int)Mathf.Round(fileWriteInterval / recordingInterval);
                var numCharsPerBlock =
                    numRecordedRows *
                    (labelLen + commaLen + m * (signCharLen + unsignedEntryLen + commaLen) + newCharLen);
                var capacity = numBlocks * numCharsPerBlock;
                // Try to optimise StringBuilder instance by pre-allocating its storage
                if (recordingTextBuilder == null)
                {
                    recordingTextBuilder = new StringBuilder(capacity);
                }
                else
                {
                    recordingTextBuilder.EnsureCapacity(capacity);
                }

                for (var i = 0; i < numRecordedRows; i++)
                {
                    recordingTextBuilder.AppendFormat("{0},", labelStr);

                    for (var j = 0; j < m; j++)
                    {
                        recordingTextBuilder.AppendFormat(
                            $"{{0:+{unsignedEntryLike};-{unsignedEntryLike}}},",
                            recordings[i * m + j]);
                    }

                    recordingTextBuilder.Append('\n');
                }
            }
        }

        private void OnDisable()
        {
            WriteToFile();
        }

        private void WriteToFile()
        {
            if (recordingTextBuilder == null) return;
            
            // Reuse the same file name for one session
            if (recordingFilePath == null)
            {
                var dirAbsPath = Path.Combine(Application.streamingAssetsPath, saveDirectoryRelativePath);
                Directory.CreateDirectory(dirAbsPath);

                var recordingFileIndex = Directory.EnumerateFiles(dirAbsPath)
                    .Count(fileName =>
                    {
                        var nameSpan = fileName.AsSpan();
                        return nameSpan.Contains(fileNameBase, StringComparison.Ordinal) &&
                               !nameSpan.Contains(".meta", StringComparison.Ordinal);
                    });

                var fileName = string.Format(fileNameFormat, fileNameBase, recordingFileIndex);
                recordingFilePath = Path.Join(dirAbsPath, fileName);
            }

            using (var csvFile = new FileStream(recordingFilePath, FileMode.Append, FileAccess.Write))
            {
                csvFile.Write(Encoding.UTF8.GetBytes(recordingTextBuilder.ToString()));
            }

            // Clear builder for next batch of text.
            recordingTextBuilder.Clear();
        }
    }
}