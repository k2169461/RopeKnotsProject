using System.Collections.Generic;
using System.Reflection;
using Oculus.Interaction;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace RopeKnotsProject.UI
{
    public class HelpImage : MonoBehaviour
    {
        [SerializeField] private Sprite[] sprites;
        private Image image;
        private int currentSpriteIndex;

        private void Awake()
        {
            image = GetComponent<Image>();
        }

        public void NextHelpImage()
        {
            image.sprite = sprites[currentSpriteIndex = (currentSpriteIndex + 1) % sprites.Length];
            image.SetNativeSize();
        }

        public void ResetScene()
        {
            // Hack to prevent missing reference exceptions and problems in grabbing interactables when the scene is reloaded.
            (typeof(OneGrabPhysicsJointTransformer)
                .GetField("_cachedGrabbingRigidbodies", BindingFlags.NonPublic | BindingFlags.Static)!
                .GetValue(null) as List<Rigidbody>)!.Clear();

            SceneManager.LoadScene(0);
        }
    }
}