1. The code base is divided between the main Unity project and the auxiliary ML.NET project titled "RopeKnotsProjectML".
2. The build supplied works on Windows builds and only supports controllers. Please use Oculus Link to test the build. The supplied video demonstrates how it works with hand interactions.
